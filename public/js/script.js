console.log("Hello there general Kenobi");

const btn = document.querySelector(".head-nav__button");
btn.insertAdjacentHTML(
    "afterbegin",
    `<i class="fas fa-bars" aria-hidden="true"></i>`
);
const menu = document.querySelector(".head-nav");
const overlay = document.createElement("div");
overlay.classList.add("overlay");
document.body.append(overlay);

btn.addEventListener("click", (e) => {
    toggleMenu(btn, toggleClass, "active", btn, menu, overlay);
});
menu.addEventListener("click", (e) => {
    toggleMenu(btn, toggleClass, "active", btn, menu, overlay);
});

overlay.addEventListener("click", (e) => {
    toggleMenu(btn, toggleClass, "active", btn, menu, overlay);
});

function toggleMenu(el, cb, ...args) {
    el.innerHTML === `<i class="fas fa-times" aria-hidden="true"></i>` ?
        (el.innerHTML = `<i class="fas fa-bars" aria-hidden="true"></i>`) :
        (el.innerHTML = `<i class="fas fa-times" aria-hidden="true"></i>`);

    cb(...args);
}

function toggleClass(className, ...elements) {
    elements.forEach((element) => {
        element.classList.toggle(className);
    });
};
